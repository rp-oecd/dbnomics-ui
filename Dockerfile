FROM node:13-alpine

RUN mkdir -p /opt
WORKDIR /opt

COPY node_modules /opt/node_modules
COPY __sapper__ /opt/__sapper__
COPY static /opt/static
COPY .env package.json /opt/

EXPOSE 3000
CMD npm start



